<?php

namespace SpipRemix\Typography;

use JoliTypo\Fixer as JoliTypoFixer;
use SpipRemix\Typography\Fixer\Fr\PunctuationMissingNoBreakSpace;

class Fixer extends JoliTypoFixer
{
    /**
     * Removed
     * - Hyphen: consuming (and not so useful).
     *
     * Added
     * - French/PunctuationMissingNoBreakSpace
     */
    public const DEFAULT_RULES_BY_LOCALE = [
        'en_GB' => ['Ellipsis', 'Dimension', 'Unit', 'Dash', 'SmartQuotes', 'NoSpaceBeforeComma', 'CurlyQuote', 'Trademark'],
        'fr_FR' => ['Ellipsis', 'Dimension', 'Unit', 'Dash', 'SmartQuotes', PunctuationMissingNoBreakSpace::class, 'FrenchNoBreakSpace', 'NoSpaceBeforeComma', 'CurlyQuote', 'Trademark'],
        'fr_CA' => ['Ellipsis', 'Dimension', 'Unit', 'Dash', 'SmartQuotes', 'NoSpaceBeforeComma', 'CurlyQuote', 'Trademark'],
        'de_DE' => ['Ellipsis', 'Dimension', 'Unit', 'Dash', 'SmartQuotes', 'NoSpaceBeforeComma', 'CurlyQuote', 'Trademark'],
    ];
}
