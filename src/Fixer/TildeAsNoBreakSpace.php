<?php

namespace SpipRemix\Typography\Fixer;

use JoliTypo\Fixer;
use JoliTypo\FixerInterface;
use JoliTypo\StateBag;

/**
 * Fixer that change tilde char `~` to non-breaking space.
 *
 * Those `~` are replaced by a no-break space, unless they are escaped with a slash, as in `\~`.
 *
 * @note
 *   Some history : tilde character is used in Tex or LaTex to represent a non-breaking space character.
 *   The SPIP editing syntax also offer this kind of writing, and potentally other tools.
 */
class TildeAsNoBreakSpace implements FixerInterface
{
    public const TILDE = '~';
    public const ESCAPED_TILDE = '\~';
    public const ESCAPER = "\x1\x14";

    public function fix($content, StateBag $stateBag = null)
    {
        if (false !== strpos($content, self::TILDE)) {
            $content = str_replace(self::ESCAPED_TILDE, self::ESCAPER, $content);
            $content = preg_replace('/['.Fixer::ALL_SPACES.']*'.self::TILDE.'+['.Fixer::ALL_SPACES.']*/mu', self::TILDE, $content);
            $content = str_replace(self::TILDE, Fixer::NO_BREAK_SPACE, $content);
            $content = str_replace(self::ESCAPER, self::TILDE, $content);
        }

        return $content;
    }
}
