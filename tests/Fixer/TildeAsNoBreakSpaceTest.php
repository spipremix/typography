<?php

namespace SpipRemix\Typography\Tests\Fixer;

use JoliTypo\FixerInterface;
use PHPUnit\Framework\TestCase;
use SpipRemix\Typography\Fixer;
use SpipRemix\Typography\Fixer\TildeAsNoBreakSpace;

class TildeAsNoBreakSpaceTest extends TestCase
{
    private FixerInterface $fixer;

    protected function setUp(): void
    {
        $this->fixer = new TildeAsNoBreakSpace();
    }

    /**
     * @dataProvider tildeAsNoBreakSpaceDataProvider
     */
    public function testTildeAsNoBreakSpace(string $in, string $expected): void
    {
        $this->assertEquals($expected, $this->fixer->fix($in));
    }

    /**
     * @dataProvider escapedTildeAsTildeDataProvider
     */
    public function testEscapedTildeAsTilde(string $in, string $expected): void
    {
        $this->assertEquals($expected, $this->fixer->fix($in));
    }

    public function tildeAsNoBreakSpaceDataProvider(): array
    {
        return [
            ['One~tilde', 'One'.Fixer::NO_BREAK_SPACE.'tilde'],
            ['This~is two~tildes', 'This'.Fixer::NO_BREAK_SPACE.'is two'.Fixer::NO_BREAK_SPACE.'tildes'],
            ['This ~ is trimmed spaces', 'This'.Fixer::NO_BREAK_SPACE.'is trimmed spaces'],
            ['This   ~   is so much not needed spaces', 'This'.Fixer::NO_BREAK_SPACE.'is so much not needed spaces'],
        ];
    }

    public function escapedTildeAsTildeDataProvider(): array
    {
        return [
            ['One\~escaped tilde', 'One~escaped tilde'],
            ['This\~is two\~escaped tildes', 'This~is two~escaped tildes'],
            ['This~is both\~escaped and~real\~tildes', 'This'.Fixer::NO_BREAK_SPACE.'is both~escaped and'.Fixer::NO_BREAK_SPACE.'real~tildes'],
        ];
    }
}
