<?php

namespace SpipRemix\Typography\Tests\Fixer\Fr;

use JoliTypo\FixerInterface;
use PHPUnit\Framework\TestCase;
use SpipRemix\Typography\Fixer;
use SpipRemix\Typography\Fixer\Fr\PunctuationMissingNoBreakSpace;

class PunctuationMissingNoBreakSpaceTest extends TestCase
{
    private FixerInterface $fixer;

    protected function setUp(): void
    {
        $this->fixer = new PunctuationMissingNoBreakSpace();
    }

    /**
     * @dataProvider questionFixDataProvider
     * @dataProvider exclamationFixDataProvider
     *
     * @dataProvider questionIgnoreDataProvider
     * @dataProvider exclamationIgnoreDataProvider
     */
    public function testFixQuestionAndExclamation(string $in, string $expected): void
    {
        $this->assertEquals($expected, $this->fixer->fix($in));
    }

    /**
     * @dataProvider SemicolonFixDataProvider
     * @dataProvider SemicolonIgnoreDataProvider
     */
    public function testFixSemicolon(string $in, string $expected): void
    {
        $this->assertEquals($expected, $this->fixer->fix($in));
    }

    /**
     * @dataProvider TwoPointsFixDataProvider
     * @dataProvider TwoPointsIgnoreDataProvider
     */
    public function testFixTwoPoints(string $in, string $expected): void
    {
        $this->assertEquals($expected, $this->fixer->fix($in));
    }

    public function questionFixDataProvider(): array
    {
        return [
            ['Quoi?', 'Quoi'.Fixer::NO_BREAK_THIN_SPACE.'?'],
            ['Quoi??', 'Quoi'.Fixer::NO_BREAK_THIN_SPACE.'??'],
            ['Quoi?!', 'Quoi'.Fixer::NO_BREAK_THIN_SPACE.'?!'],
            ['Quoi?!.', 'Quoi'.Fixer::NO_BREAK_THIN_SPACE.'?!.'],
            ['Quoi?!.', 'Quoi'.Fixer::NO_BREAK_THIN_SPACE.'?!.'],
        ];
    }

    public function questionIgnoreDataProvider(): array
    {
        return [
            ['Quoi', 'Quoi'],
            ['?Quoi', '?Quoi'],
            ['??Quoi', '??Quoi'],
            ['Mais-?', 'Mais-?'],
            ['Mais :-?', 'Mais :-?'],
        ];
    }

    public function exclamationFixDataProvider(): array
    {
        return [
            ['Quoi!', 'Quoi'.Fixer::NO_BREAK_THIN_SPACE.'!'],
            ['Quoi!?', 'Quoi'.Fixer::NO_BREAK_THIN_SPACE.'!?'],
            ['Quoi!!', 'Quoi'.Fixer::NO_BREAK_THIN_SPACE.'!!'],
            ['Quoi!!.', 'Quoi'.Fixer::NO_BREAK_THIN_SPACE.'!!.'],
            ['Quoi!!.', 'Quoi'.Fixer::NO_BREAK_THIN_SPACE.'!!.'],
        ];
    }

    public function exclamationIgnoreDataProvider(): array
    {
        return [
            ['Quoi', 'Quoi'],
            ['!Quoi', '!Quoi'],
            ['!?Quoi', '!?Quoi'],
            ['Mais-!', 'Mais-!'],
            ['Mais :-!', 'Mais :-!'],
        ];
    }

    public function semicolonFixDataProvider(): array
    {
        return [
            ['Un chien;', 'Un chien'.Fixer::NO_BREAK_THIN_SPACE.';'],
            ['Un chien; un chat.', 'Un chien'.Fixer::NO_BREAK_THIN_SPACE.'; un chat.'],
        ];
    }

    public function semicolonIgnoreDataProvider(): array
    {
        return [
            [';au commencement', ';au commencement'],
            ['; au commencement', '; au commencement'],
        ];
    }

    public function twoPointsFixDataProvider(): array
    {
        return [
            ['Mon choix:', 'Mon choix'.Fixer::NO_BREAK_SPACE.':'],
            ['Mon choix: simplement', 'Mon choix'.Fixer::NO_BREAK_SPACE.': simplement'],
        ];
    }

    public function twoPointsIgnoreDataProvider(): array
    {
        return [
            ['un :smiley: joli', 'un :smiley: joli'],
            ['Pour 18:35 nous partons.', 'Pour 18:35 nous partons.'],
            ['Beau :-)', 'Beau :-)'],
            ['IP ::1', 'IP ::1'],
            ['IP fdda:5cc1:23:4::1f', 'IP fdda:5cc1:23:4::1f'],
            ['Lien https://fr.wikipedia.org/wiki/Code_typographique', 'Lien https://fr.wikipedia.org/wiki/Code_typographique'],
            [': au commencement', ': au commencement'],
            [':au commencement', ':au commencement'],
        ];
    }
}
