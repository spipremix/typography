<?php

namespace SpipRemix\Typography\Fixer\Fr;

use JoliTypo\Fixer;
use JoliTypo\FixerInterface;
use JoliTypo\StateBag;

/**
 * Fixer that add a space before a pounctuation mark when this is needed,
 * like on `Quoi!`, `Question?` or `Elle disait:`, but missing.
 *
 * It has to be called before JoliTypo\FrenchNoBreakSpace which correct
 * the space type when it already exists (like on `Quoi !`), but does not
 * add space when it’s missing.
 *
 * It try to be careful with some situations, like `12:30` or `:smiley:`
 * that don’t need it.
 *
 * Add
 * - NO_BREAK_SPACE before :
 * - NO_BREAK_THIN_SPACE before ; : ! ?
 */
class PunctuationMissingNoBreakSpace implements FixerInterface
{
    public const ESCAPER = "-\x2-";

    public function fix($content, StateBag $stateBag = null)
    {
        $content = $this->fixQuestionAndExclamation($content);
        $content = $this->fixSemicolon($content);
        $content = $this->fixTwoPoints($content);

        return $content;
    }

    /**
     * One or more !? need a thin space before.
     */
    protected function fixQuestionAndExclamation(string $content): string
    {
        $content = preg_replace('@(\w)['.Fixer::ALL_SPACES.']*([!\?][!\?\.]*)@mu', '$1'.Fixer::NO_BREAK_THIN_SPACE.'$2', $content);

        return $content;
    }

    /**
     * One ; need a thin space before, but… not en entities `&#123;`, nor on `;start`.
     *
     * @note regexp cannot use Negative Lookbehind with variable size content, so more steps…
     */
    protected function fixSemicolon(string $content): string
    {
        if (false !== strpos($content, ';')) {
            // Escape ; from entities. And in start of line.
            $content = preg_replace('@(&#?[0-9a-z]+|^);@mui', '$1'.self::ESCAPER, $content);
            // Escape`;-/` or `;truc`
            $content = preg_replace('@;(\w|[[:punct:]])@mu', self::ESCAPER.'$1', $content);
            // Add the space…
            $content = preg_replace('@['.Fixer::ALL_SPACES.']*;()@mu', Fixer::NO_BREAK_THIN_SPACE.';', $content);
            // Restore escaping
            $content = str_replace(self::ESCAPER, ';', $content);
        }

        return $content;
    }

    /**
     * One : need a space before…
     * but not on multiple ::, nor :smiley: nor 12:30 hour….
     */
    protected function fixTwoPoints(string $content): string
    {
        if (false !== strpos($content, ':')) {
            // Escape :smileys: or just ::
            $content = preg_replace('@:(\w*):@mu', self::ESCAPER.'$1'.self::ESCAPER, $content);
            // Escape 12:30 hours or :truc or :-/ or :// ...
            $content = preg_replace('@:(\w|[[:punct:]])@mu', self::ESCAPER.'$1', $content);
            // Escape : in start of line.
            $content = preg_replace('@^:@mui', self::ESCAPER, $content);
            // Add a space before :
            $content = preg_replace('@['.Fixer::ALL_SPACES.']*(:)@mu', Fixer::NO_BREAK_SPACE.'$1', $content);
            // Restore escaping
            $content = str_replace(self::ESCAPER, ':', $content);
        }

        return $content;
    }
}
