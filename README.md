# SpipRemix/Typography

SpipRemix Typography is a tool that provide microtypography corrections for texts.
It relies on [JoliCode/JoliTypo](https://github.com/jolicode/JoliTypo) library with additional rules.

This library can be used in any PHP project.
For usage alongside the SPIP CMS, see [SpipRemix/Typographer](https://gitlab.com/spipremix/typographer) library.

## Installation

```sh
composer require spipremix/typography
```

## Usage

The integrated Fixer class extends the native JoliTypo’s one, so the syntax is similar. See [JoliTypo](https://github.com/jolicode/JoliTypo) for more informations.

```php
use SpipRemix/Typography/Fixer;

$rules = Fixer::DEFAULT_RULES_BY_LOCALE['fr_FR'];
$fixer = new Fixer($rules);
$fixer->setLocale('fr_FR');

$htmlContent = $fixer->fix($htmlContent);
$stringContent = $fixer->fixString($stringContent);
```

### constant DEFAULT_RULES_BY_LOCALE

We have introduced `Fixer::DEFAULT_RULES_BY_LOCALE` constant which differ from native `Fixer::RECOMMENDED_RULES_BY_LOCALE` in that way :

- **Removed Hyphen** rule :
This rule si notably time consuming. By the way it becomes less useful as CSS gets an hyphenation support (not on all browsers nor languages yet however).
- **Added Fr/PunctuationMissingNoBreakSpace** rule for `fr_FR` locale :
Unlike JoliTypo, this rule adds non-breaking space beetween a word and a punctuation sign when it’s missing and needed. Native JoliTypo only alter the string for a non-breaking space if a space already exists. See [JoliTypo#31 issue](https://github.com/jolicode/JoliTypo/issues/31)

### Load additional rules

```php
use SpipRemix/Typography/Fixer;
use SpipRemix/Typography/Fixer/TildeAsNoBreakSpace;

$rules = Fixer::DEFAULT_RULES_BY_LOCALE['fr_FR'];
// adds TildeAsNoBreakSpace in first place.
$rules = [TildeAsNoBreakSpace::class, ...$rules];
$fixer = new Fixer($rules);
$fixer->setLocale('fr_FR');
```


## Additional Rules

We introduced some additional rules to upgrade some part of the JoliTypo ones.


### TildeAsNoBreakSpace

Fixer that change tilde char `~` to non-breaking space, unless they are escaped with a slash, as in `\~`.

**Note:** Tilde character is used in Tex or LaTex to represent a non-breaking space character. The SPIP editing syntax also offer this kind of writing, and potentally other tools.

This rule, somehow specific, is not in the `Fixer::DEFAULT_RULES_BY_LOCALE` rules list.


### Fr/PunctuationMissingNoBreakSpace

This rule completes the `FrenchNoBreakSpace` native one, and should be added before it in the rules order.

This rule adds non-breaking space beetween a word and a punctuation sign when it’s missing and needed, like in `Quoi?`, `Quoi?` or `Quoi:`. Native JoliTypo only replaces the string for a non-breaking space if a space already exists, like in `Quoi ?`.

This rule try to be smart in some edge cases by not touching specific patterns like `12:34`, `:happy:`, `:-)` an so on.